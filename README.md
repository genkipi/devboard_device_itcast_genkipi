# 传智教育元气派GenkiPi

- [简介](#section1)
- [目录](#section2)
- [编译指导](#section3)



## 简介<a name="section1"></a>

元气派GenKiPi开发板由传智播客教育科技股份有限公司出品，搭载OpenHarmony2.0操作系统；基于海思HI3861模组, 160MHz主频、SRAM 352KB、ROM 288KB、2M Flash。支持SPI、IIC、UART、ADC、PWM等开发协议， 广泛适用于智能穿戴、智能安防和工业物联网场景。

![](./asserts/board.png)

|  引脚   |      Uart       |  SPI  |  ADC  |  PWM  |  I2S  | SDIO |  I2C  |
| :-----: | :-------------: | :---: | :---: | :---: | :---: | :--: | :---: |
| GPIO_07 |      CTS_1      | RXD_0 | ADC_3 | PWM_0 | CLK_0 |      |       |
| GPIO_08 |      RTS_1      | TXD_0 |       | PWM_1 | WS_0  |      |       |
| GPIO_10 |      CTS_2      | CLK_0 |       | PWM_1 | TX_0  |  D3  | SDA_0 |
| GPIO_09 |      RTS_2      | TXD_0 | ADC_4 | PWM_0 | MCK_0 |  D2  | SCL_0 |
| GPIO_03 |    LOG_TXD_0    |       |       |       |       |      |       |
| GPIO_04 |    LOG_RXD_0    |       | ADC_1 |       |       |      |       |
| GPIO_02 |                 |       |       | PWM_2 | MCK_0 |      |       |
| GPIO_05 |      RXD_1      | CSI_0 | ADC_2 | PWM_2 | TX_0  |      |       |
| GPIO_06 |      TXD_1      | CLK_0 |       | PWM_3 |       |      |       |
| GPIO_14 | LOG_RXD_0/CTS_2 |       |       | PWM_5 | RX_0  |  D1  | SCL_0 |
| GPIO_11 |      TXD_2      | RXD_0 | ADC_5 | PWM_2 | CLK_0 | CMD  |       |
| GPIO_12 |      RXD_2      | CSI_0 | ADC_0 | PWM_3 | WS_0  | CLK  |       |
| GPIO_13 | LOG_TXD_0/RTS_2 |       | ADC_6 | PWM_4 |       |  D0  | SDA_0 |





## 目录<a name="section2"></a>

GenkiPi 的SDK软件包根目录所在位姿为`device/itcast/genkipi/`, 如下所示:

```bash
device/itcast/genkipi
├── BUILD.gn			# GN构建脚本
├── interfaces			# 自定义接口
└── sdk_liteos			# Liteos内核目录
    ├── app				# 应用层代码（其中包含demo程序为参考示例）。
    ├── boot			# Flash bootloader代码。
    ├── build			# SDK构建所需的库文件、链接文件、配置文件。
    ├── BUILD.gn		# GN构建脚本
    ├── build_patch.sh	# 用于解压uboot开源源码包和打patch。
    ├── build.sh		# 启动编译脚本，同时支持“sh build.sh。 menuconfig”进行客制化配置。
    ├── components		# SDK平台相关的组件。
    ├── config			# SDK系统配置文件。
    ├── config.gni		# 支持OpenHarmony配置文件。
    ├── factory.mk		# 厂测版本编译脚本。
    ├── hm_build.sh		# 适配OpenHarmony构建脚本。
    ├── include			# API头文件存放目录。
    ├── license			# SDK开源license声明。
    ├── Makefile		# 支持make编译，使用“make”或“make all”启动编译。
    ├── non_factory.mk	# 非厂测版本编译脚本。
    ├── platform		# SDK平台相关的文件（包括：内核镜像、驱动模块等）。
    ├── SConstruct		# SCons编译脚本。
    ├── third_party		# 开源第三方软件目录。
    └── tools			# SDK提供的Linux系统和Windows系统上使用的工具（包括：NV制作工具、签名工具、Menuconfig等）。
```



## 编译指导<a name="section3"></a>

### 源码下载

1. repo工具准备。如果已经安装过repo工具，此步骤省略

   ```bash
   curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo
   sudo mv repo /usr/local/bin/repo
   sudo chmod a+x /usr/local/bin/repo
   python3 -m pip install -i https://repo.huaweicloud.com/repository/pypi/simple requests
   ```

2. 源码下载

   ```bash
   repo init -u https://gitee.com/openharmony-sig/manifest.git -b master --no-repo-verify -m devboard_itcast_genkipi.xml
   repo sync -c
   repo forall -c 'git lfs pull'
   ```

### 编译代码

1. 选择开发板

   ```bash
   hb set
   ```

   选中 itcast 下的genkipi 即可

2. 编译

   ```bash
   hb build
   ```

### 烧录

通过 hiburn 工具将 out 目录下 allinOne.bin文件进行烧录



