/*
 * Copyright (c) 2021 Itcast., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GENKIPI_GENKI_WIFI_STA_H
#define GENKIPI_GENKI_WIFI_STA_H

/**
 * link wifi
 * @param ssid
 * @param password
 * @param hostname
 * @return 0 failed 1 success
 */
int connect_wifi(const char* ssid, const char* password, const char* hostname);
/**
 * disconnect wifi
 * @return 0 failed 1 success
 */
int disconnect_wifi(void);

/**
 * get wifi addr
 * @param ip
 * @param netmask
 * @param gateway
 * @return
 */
int get_wifi_addr(unsigned int *ip, unsigned int *netmask, unsigned int *gateway);

#endif //GENKIPI_GENKI_WIFI_STA_H
