# Itcast GenkiPi

- [Introduction](#section1)
- [Directory Structure](#section2)
- [Compilation](#section3)



## Introduction<a name="section1"></a>

GenkiPi development board is produced by Itcast  Co., Ltd. It is equipped with OpenHarmony2.0 operating system; based on Hisilicon HI3861 module, 160MHz main frequency, SRAM 352KB, ROM 288KB, 2M Flash. 。It supports development protocols such as SPI, IIC, UART, ADC, and PWM, and is widely used in smart wear, smart security and industrial IoT scenarios.

![](./asserts/board.png)

|  引脚   |      Uart       |  SPI  |  ADC  |  PWM  |  I2S  | SDIO |  I2C  |
| :-----: | :-------------: | :---: | :---: | :---: | :---: | :--: | :---: |
| GPIO_07 |      CTS_1      | RXD_0 | ADC_3 | PWM_0 | CLK_0 |      |       |
| GPIO_08 |      RTS_1      | TXD_0 |       | PWM_1 | WS_0  |      |       |
| GPIO_10 |      CTS_2      | CLK_0 |       | PWM_1 | TX_0  |  D3  | SDA_0 |
| GPIO_09 |      RTS_2      | TXD_0 | ADC_4 | PWM_0 | MCK_0 |  D2  | SCL_0 |
| GPIO_03 |    LOG_TXD_0    |       |       |       |       |      |       |
| GPIO_04 |    LOG_RXD_0    |       | ADC_1 |       |       |      |       |
| GPIO_02 |                 |       |       | PWM_2 | MCK_0 |      |       |
| GPIO_05 |      RXD_1      | CSI_0 | ADC_2 | PWM_2 | TX_0  |      |       |
| GPIO_06 |      TXD_1      | CLK_0 |       | PWM_3 |       |      |       |
| GPIO_14 | LOG_RXD_0/CTS_2 |       |       | PWM_5 | RX_0  |  D1  | SCL_0 |
| GPIO_11 |      TXD_2      | RXD_0 | ADC_5 | PWM_2 | CLK_0 | CMD  |       |
| GPIO_12 |      RXD_2      | CSI_0 | ADC_0 | PWM_3 | WS_0  | CLK  |       |
| GPIO_13 | LOG_TXD_0/RTS_2 |       | ADC_6 | PWM_4 |       |  D0  | SDA_0 |





## Direction Structure<a name="section2"></a>

The root directory of  GenkiPi SDK package is  `device/itcast/genkipi/`:

```bash
device/itcast/genkipi
├── BUILD.gn # GN build script
├── interfaces # custom interface
└── sdk_liteos # Liteos kernel directory
    ├── app # Application layer code (including demo program as a reference example).
    ├── boot # Flash bootloader code.
    ├── build # The library files, link files, and configuration files needed for SDK building.
    ├── BUILD.gn # GN build script
    ├── build_patch.sh # Used to decompress uboot open source source package and patch.
    ├── build.sh # Start the compilation script and support "sh build.sh. menuconfig" for customized configuration.
    ├── components # SDK platform related components.
    ├── config # SDK system configuration file.
    ├── config.gni # Support OpenHarmony configuration file.
    ├── factory.mk # The factory test version compilation script.
    ├── hm_build.sh # Adapt to OpenHarmony build script.
    ├── include # API header file storage directory.
    ├── license # SDK open source license statement.
    ├── Makefile # Support make compilation, use "make" or "make all" to start compilation.
    ├── non_factory.mk # Non-factory test version compilation script.
    ├── platform # SDK platform related files (including: kernel image, driver module, etc.).
    ├── SConstruct # SCons compile script.
    ├── third_party # Open source third-party software directory.
    └── tools # Tools provided by the SDK on Linux and Windows systems (including: NV creation tools, signature tools, Menuconfig, etc.).
```



## Compilation<a name="section3"></a>

### Source Download

1. repo tool install。if had install it , omit this step.

   ```bash
   curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo
   sudo mv repo /usr/local/bin/repo
   sudo chmod a+x /usr/local/bin/repo
   python3 -m pip install -i https://repo.huaweicloud.com/repository/pypi/simple requests
   ```

2. download source

   ```bash
   repo init -u https://gitee.com/openharmony-sig/manifest.git -b master --no-repo-verify -m devboard_itcast_genkipi.xml
   repo sync -c
   repo forall -c 'git lfs pull'
   ```

### Compile Code

1. choose devboard

   ```bash
   hb set
   ```

   choose itcast  genkipi

2. compile

   ```bash
   hb build
   ```

### Burn Bin

burn bin with Hiburn tool.





